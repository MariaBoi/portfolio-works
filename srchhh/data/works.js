import { images, toolIcons } from "../constants";

export const works = [
  {
    title: "Streaming service",
    description: `presenting part of streaming service, user can watch movies
     list, details of single show, 
      filter movies by different categories,
       register and log in, add movies to favorites add movies to favorites `,
    imgUrl: images.stream,
    projectLink: "https://streaming-service2.vercel.app",
    codeLink: "https://gitlab.com/MariaBoi/streaming-service2",
    tags: ["React JS"],
    tools: [toolIcons.AiFillQqCircle],
  },
  {
    title: "Table with memoization",
    description: `It is web app main purpose of which is using memoization working with large-sized table, 
    it highlights values(amount user can specify in X-input) most close to cell, 
    user is hovering, memoization helps boost performance, 
    since react does not re-render every cell, but only the cells whose color should change.
    At the end of each row there is a cell with the sum of all elements in a row, 
    when hovering on this cell all cells in this row change percentage of its contribution to the total sum of the row
    Below each column the is a cell which shows average amount of the column.
    After clicking the cell in table value of a cell is incremented by one, sum of the roe and average value of column also changes.`,
    imgUrl: images.table,
    projectLink: "https://table-henna.vercel.app",
    codeLink: "https://gitlab.com/MariaBoi/streaming-service2",

    tags: ["React JS"],
  },

  {
    title: "app for passing programming tests",
    description: `its is website main purpose of it is passing tests on programing
     languages(makes code formatted like in code editor) has big descriptive main page with good design. 
  possibility to chose some category and then tests
      from this category, separate pages for all tests, and all categories, and test by one categories, for 
      can perform both multi-answer question as well as on answer question. For convenience added breadcrumb component,
       so user an go back to previous pages`,
    imgUrl: images.tests,
    projectLink: "https://interviewboom.com/",
    codeLink:
      "https://bitbucket.org/modelfak27/interview-public/commits/branch/develop",

    tags: ["React JS"],
  },
];
