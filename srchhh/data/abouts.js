import { images } from "../constants";

export const abouts = [
  {
    title: "web design ",
    description: `I'm passionate about bringing both the technical and visual aspects of digital products to life. 
    User experience, beautiful pixels and writing clean accessible, human code matters to me.`,
    imageUrl: images.about01,
  },
  {
    title: "SEO target ads",
    description: ` I sweat the details. And as a follower of John Maeda’s Laws Of Simplicity,
     I agree that less is more.`,
    imageUrl: images.about02,
  },
  {
    title: "Smm",
    description: `I have a BA in Multimedia Design from Curtin University 🇦🇺,
     a Certificate of Web Development Immersive from Juno College (Formerly HackerYou) 🇨🇦,
     (and an Advanced Scuba Diving License from PADI 🇵🇭!)`,
    imageUrl: images.about03,
  },
  {
    title: "copy writing",
    description: `I'm happiest when I'm creating, learning, exploring and thinking about how to make things better. 
    Currently solving deceptively simple problems at UsePastel.Com.
     I'm not available for any freelance work, but feel free to reach out and say hello! I`,
    imageUrl: images.about04,
  },
];
