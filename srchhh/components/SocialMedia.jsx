import React from "react";
import { BsInstagram } from "react-icons/bs";
import { FaSass, FaJs, FaHtml5, FaReact } from "react-icons/fa";
import { AiFillQqCircle } from "react-icons/ai";

const SocialMedia = ({ tools }) => (
  <div className="app__social">
    <div>
      <FaSass />
    </div>
    <div>
      <FaJs />
    </div>
    <div>
      <FaJs />
    </div>
    <div>
      <FaHtml5 />
    </div>
    <div>
      <FaReact />
    </div>
    {console.log(tools, "jjjj")}
    {tools?.map((Component, key) => (
      <div>
        <Component key={key} />
      </div>
    ))}
  </div>
);

export default SocialMedia;
