import { images, toolIcons } from "../constants";

export const works = [
  {
    title: "App for passing programming tests",
    description: `\tIt is a website main purpose of it is passing tests on programing languages(makes code formatted like in code editor)
      \tIt has a big descriptive main page with possibility to chose some category and then tests from this category
       \tThere are separate pages for all tests, and all categories, and test by one categories
can perform both multi-answer questions as well as one answer question.
\tFor convenience and navigation throughout pages  added breadcrumb component,so user an go back to previous pages 
     (currently it is not deployed because has backend developer is responsible for deployment)`,
    imgUrls: [images.tests2, images.tests1, images.tests4],
    projectLink: "",
    codeLink: "https://bitbucket.org/modelfak27/interview-public/src/develop/",

    tags: ["React JS"],
    video: "",
    tools: [
      toolIcons.TbBrandNextjs,
      toolIcons.SiTypescript,
      toolIcons.TbBrandSass,
    ],
  },
  {
    title: "Streaming service",
    description: `\tIt is a presenting part of streaming service. user can watch movies list, details of single show, filter movies by different categories, he can choose several categories to filter.
   \tAlso can register and log in, only registered users can add movies to favorites add movies to favorites, 
\tUser  has private cabinet where they can watch list of movies, he liked. 
\tYou can also see the list of all registered users `,
    imgUrls: [images.stream, images.stream2, images.stream3],
    projectLink: "https://stoic-ride-3a36c6.netlify.app/",
    codeLink: "https://gitlab.com/MariaBoi/streaming-service2",
    tags: ["React JS"],
    tools: [toolIcons.DiNodejs],
  },
  {
    title: "Table with memoization",
    description: `\tIt is a web app main purpose of which is using memoization working with large-sized table, 
    \tIt highlights values(amount user can specify in X-input) most close to cell, user is hovering, memoization helps boost performance, since react does not re-render every cell, but only the cells whose color should change.
\tAt the end of each row there is a cell with the sum of all elements in a row,
when hovering on this cell all cells in this row change percentage of its contribution to the total sum of the row
\tBelow each column the is a cell which shows average amount of the column.
\tAfter clicking the cell in table value of a cell is incremented by one, sum of the roe and average value of column also changes.`,
    imgUrls: [images.table, images.table2, images.table3],
    projectLink: "https://table-henna.vercel.app",
    codeLink: "https://github.com/akieve/table",

    tags: ["React JS"],
    tools: [toolIcons.SiTypescript],
  },
  {
    title: "E-commerce website",
    description: `\tApp has a dynamic display of product cards (fakestore api used),
   \tproducts are filtered by category (with a change in the url of the category parameters)
   \tUser can switch the view of the grid - 4 or 2 in a row (with a change in the url of the category parameters)
\tThere is a pagination of the catalog by changing the url parameter of the page.
 \t User can see products he selected (implemented via localStorage). 
\tResponsive layout is present`,
    imgUrls: [images.shop1, images.shop2, images.shop3],
    projectLink: "https://shop-7zoj.vercel.app",
    codeLink: "https://github.com/akieve/shop",

    tags: ["React JS"],
    tools: [toolIcons.TbBrandSass],
  },
];
